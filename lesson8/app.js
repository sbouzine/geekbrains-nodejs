'use strict';

const path = require('path');
const colors = require('colors');
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

// options
const port = 8008;

// set public directory with js, css
app.use(express.static(
	path.join(__dirname, 'public')
));


// routes
app.get('/', (req, res) => {
	res.redirect('/client.html');
});

// websockets
io.sockets.on('connection', (socket) => {
	console.log('New connection: ' + socket.id);
	socket.on('message', (data) => {
		io.sockets.emit('message', data);
		console.log(data);
	});
	socket.on('in', (data) => {
		io.sockets.emit('in', data);
	});
	socket.on('out', (data) => {
		io.sockets.emit('out', data);
	});
	socket.on('disconnect', () => {
		console.log('Disconnected: ' + socket.id);
	});

});


// start app
console.log('Lesson 8'.bold);
console.log('WebSockets'.yellow);
console.log('Server started on port ' + port);
server.listen(port);