var ansi = require('ansi');
var cursor = ansi(process.stdout);
var delay = require('delay');

cursor.bold().write('Lesson 1\n').reset();

delay(500).then(() => {
	cursor.red().write('3\n').beep().reset();
}).then(delay(500)).then(() => {
	cursor.yellow().write('2\n').beep().reset();
}).then(delay(500)).then(() => {
	cursor.green().write('1\n').beep().reset();
}).then(delay(500)).then(() => {
	cursor.bold().write('GO!\n').reset();
});
