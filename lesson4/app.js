'use strict';

const request = require('request');
const cheerio = require('cheerio');
const colors = require('colors');
const templating = require('consolidate');
const handlebars = require('handlebars');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const express = require('express');
const app = express();

// options
const port = 8008;

// use cookie-parser and body-parser
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended:true}));

// use handlebars as templating
app.engine('hbs', templating.handlebars);
app.set('view engine', 'hbs');
app.set('views', __dirname);

// required to use registerHelper function
templating.requires.handlebars = handlebars;



app.get('/', (req, res) => {

	let options = {
		hub: req.cookies.hub == undefined ? null : req.cookies.hub,
		pages: req.cookies.pages == undefined ? null : req.cookies.pages,
	};

	res.render('index', {
		title: 'Express Application',
		options: options,
		posts: []
	});
});


app.post('/', (req, res) => {

	// set cookies
	res.cookie('hub', req.body.hub);
	res.cookie('pages', req.body.pages);

	let options = {
		hub: req.body.hub,
		pages: req.body.pages
	};

	loadHabraNews(options, (data) => {
		res.render('index', {
			title: 'Express Application',
			options: options,
			posts: data
		});
	});

});


// start app
console.log('Lesson 4'.bold);
console.log('Express Application'.yellow);
console.log('Server started on port ' + port);
app.listen(port);




/*--------------- functions ----------------*/

// set selected option in select tag
handlebars.registerHelper('select', function(value, options) {
	let pattern = new RegExp(' value=\"' + value + '\"');
	return options.fn(this).replace(pattern, '$& selected');
});



function loadHabraNews(options, callback) {

	// default options
	if(options.hub === undefined)
		options.hub = null;
	if(options.pages === undefined)
		options.pages = 1;
	if(options.page === undefined)
		options.page = 1;
	if(options.data === undefined)
		options.data = [];

	let url = 'http://habrahabr.ru/';
	url += options.hub ? 'hub/' + options.hub : 'interesting/';
	url += '/page' + options.page;

	request(url, (error, response, body) => {
		if(error) {
			console.log(error);
			return;
		}

		if(response.statusCode != 200)
			return;

		// cheerio with decodeEntities to show cyrillic symbols
		let $ = cheerio.load(body, {decodeEntities: true});

		$('.posts_list .post_title').each(function() {
			options.data.push({
				text: $(this).text(),
				href: $(this).attr('href')
			});
		});

		if(++options.page <= options.pages)
			loadHabraNews(options, callback);
		else
			callback(options.data);

	});

}
