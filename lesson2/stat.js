var argv = require('minimist')(process.argv.slice(2));
var colors = require('colors');
var fs = require('fs');


// check for file with results
var	file_stat = argv['_'][0] || 'stat.txt';
fs.access(file_stat, (err) => {
	if(err) {
		console.log('Stat file not found'.red);
		process.exit(1);
	}
});


console.log('Lesson 2'.bold);
console.log(('Stats from ' + file_stat + ':').yellow);


fs.readFile(file_stat, (err, data) => {

	if(err)
		throw err;

	// Convert data to string which consists of 0 (losses) and 1 (wins)
	data = data.toString().trim().split('\r\n').map((val) => {
		val = val.split(';');
		return val[0] == val[1] ? 1 : 0;
	}).join('');

	var rounds = data.length;
	var wins = data.split(1).length - 1;
	var losses = rounds - wins;
	var persent_win = Math.round(wins / rounds * 100);
	var persent_loss = Math.round(losses / rounds * 100);

	var win_streak = data.split(0).reduce((carry, val) => {
		return Math.max(carry, val.length);
	}, 0);

	var loss_streak = data.split(1).reduce((carry, val) => {
		return Math.max(carry, val.length);
	}, 0);


	console.log('Rounds: ' + rounds);
	console.log('Wins: ' + wins + ' (' + persent_win + '%)');
	console.log('Losses: ' + losses + ' (' + persent_loss + '%)');
	console.log('Max Winning Streak: ' + win_streak);
	console.log('Max Losing Streak: ' + loss_streak);
	console.log();

});