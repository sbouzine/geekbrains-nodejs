var prompt = require('prompt');
var argv = require('minimist')(process.argv.slice(2));
var colors = require('colors');
var fs = require('fs');


// prompt settings
prompt.message = '';
prompt.delimiter = '';
var prompt_schema = {
	properties: {
		choise: {
			description: 'Choose your destiny: '.white,
			pattern: /^([01]|stop)$/,
			required: true
		}
	}
};


// file with results
var	file_stats = argv['_'][0] || 'stat.txt';


// application start
console.log('Lesson 2'.bold);
console.log('Heads or tails (controls: 0/1 or "stop" to exit)'.yellow);
prompt.start();

play(prompt_schema, file_stats);



/*--------------- functions ----------------*/

function play(schema, file) {
	prompt.get(schema, (err, result) => {
		if(err)
			throw err;
		if(result.choise == 'stop') {
			console.log();
			process.exit(0);
		}
		var value = heads_or_tails();
		var data = [value, result.choise];
		fs.appendFileSync(file, data.join(';') + '\r\n');
		value == result.choise
			? console.log('win'.green)
			: console.log('loss'.blue);
		play(schema, file);
	});
}


function heads_or_tails() {
	return Math.round(Math.random());
}