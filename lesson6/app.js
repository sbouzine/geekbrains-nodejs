'use strict';

const path = require('path');
const colors = require('colors');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('cookie-session');
const templating = require('consolidate');
const jade = require('jade');
const express = require('express');
const app = express();

// options
const port = 8008;

// load models and controllers
const config = require('./config.js');
const pool = require('./support/mysql.js')(config);
const userModel = require('./models/user.js')(pool);
const articleModel = require('./models/article.js')(pool);
const articleController = require('./controllers/article.js')(articleModel, userModel);

// body, cookie, session
app.use(cookieParser());
app.use(session({keys: ['secret']}));
app.use(bodyParser.urlencoded({extended:true}));

// use passport
const auth = require('./support/passport')(app, userModel);

// check auth
const isAuth = require('./support/isauth');

// use jade as templating
app.engine('jade', templating.jade);
app.set('view engine', 'jade');
app.set('views', __dirname + '/templates');

// set public directory with js, css
app.use(express.static(
	path.join(__dirname, 'public')
));


// routes
app.get('/', (req, res) => {
	req.isAuthenticated() ? res.redirect('/article') : res.redirect('/login');
});

app.get('/login', (req, res) => {
	req.isAuthenticated() ? res.redirect('/article') : res.render('login');
});

app.post('/login', auth);

app.get('/logout', (req, res) => {
	req.logout();
	res.redirect('/');
});


// article routes
app.all('/article', isAuth);
app.all('/article/*', isAuth);
app.get('/article', (req, res) => articleController.index(req, res));
app.get('/article/add', (req, res) => articleController.addForm(req, res));
app.get('/article/edit/:id', (req, res) => articleController.editForm(req, res));
app.get('/article/:id', (req, res) => articleController.view(req, res));
app.post('/article/add', (req, res) => articleController.add(req, res));
app.post('/article/edit/:id', (req, res) => articleController.edit(req, res));
app.post('/article/delete/:id', (req, res) => articleController.delete(req, res));


// start app
console.log('Lesson 6'.bold);
console.log('Authentication'.yellow);
console.log('Server started on port ' + port);
app.listen(port);
