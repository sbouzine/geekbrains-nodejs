'use strict';


class ArticleController {

	constructor(model, userModel) {
		this.model = model;
		this.userModel = userModel;
	}

	// get actions
	index(req, res) {
		this.userModel.list((users) => {
			this.model.list((data) => {
				res.render('article_index', {
					title: 'Authentication',
					articles: data,
					users: users,
					user: {
						id: req.session.passport.user,
						name: users[req.session.passport.user]
					},
				});
			});
		});
	}

	view(req, res) {
		this.userModel.get(req.session.passport.user, (user) => {
			this.model.get(req.params.id, (data) => {
				res.render('article_view', {
					title: 'Authentication',
					article: data,
					user: user
				});
			});
		});
	}

	addForm(req, res) {
		this.userModel.get(req.session.passport.user, (user) => {
			res.render('article_form', {
				title: 'Authentication',
				article: {name: '', content: ''},
				action: '/article/add',
				user: user
			});
		});
	}

	editForm(req, res) {
		this.userModel.get(req.session.passport.user, (user) => {
			this.model.get(req.params.id, (data) => {
				res.render('article_form', {
					title: 'Authentication',
					article: data,
					action: '/article/edit/' + data.id,
					user: user
				});
			});
		});
	}

	// post actions
	add(req, res) {
		this.userModel.get(req.session.passport.user, (user) => {
			let article = req.body.article;
			article.user_id = user.id;
			this.model.add(article, (insertId) => {
				res.redirect('/article/' + insertId);
			});
		});
	}

	edit(req, res) {
		this.model.edit(req.params.id, req.body.article, () => {
			res.redirect('/article/' + req.params.id);
		});
	}

	delete(req, res) {
		this.model.delete(req.params.id, () => {
			res.redirect('/article');
		});
	}

}



module.exports = (model, userModel) => {
	return new ArticleController(model, userModel);
};