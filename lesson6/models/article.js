'use strict';


class Article {

	constructor(pool) {
		this.db = pool;
		this.table = 'articles';
	}

	list(callback) {
		this.db.query('SELECT * FROM ??', [this.table], (data) => {
			callback(data);
		});
	}

	get(id, callback) {
		this.db.query('SELECT * FROM ?? WHERE id=?', [this.table, id], (data) => {
			callback(data[0]);
		});
	}

	add(data, callback) {
		this.db.query('INSERT INTO ?? SET ?', [this.table, data], (result) => {
			callback(result.insertId);
		});
	}

	edit(id, data, callback) {
		this.db.query('UPDATE ?? SET ? WHERE id=?', [this.table, data, id], (result) => {
			callback();
		});
	}

	delete(id, callback) {
		this.db.query('DELETE FROM ?? WHERE id=?', [this.table, id], (result) => {
			callback();
		});
	}

	// gets fields in table
	fields(callback) {
		this.db.query('SHOW fields FROM ??', [this.table], (result) => {
			callback(result.map((value) => {
				return value.Field;
			}));
		});
	}

}


module.exports = (pool) => {
	return new Article(pool);
};