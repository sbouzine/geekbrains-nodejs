'use strict';


class User {

	constructor(pool) {
		this.db = pool;
		this.table = 'users';
	}

	list(callback) {
		this.db.query('SELECT * FROM ??', [this.table], (data) => {
			let users = {};
			data.forEach((value) => {
				users[value.id] = value.name;
			});
			callback(users);
		});
	}

	get(id, callback) {
		this.db.query('SELECT * FROM ?? WHERE id=?', [this.table, id], (data) => {
			callback(data[0]);
		});
	}

	auth(login, password, callback) {
		this.db.query('SELECT * FROM ?? WHERE mail=? AND password=?',
		[this.table, login, password], (data) => {
			callback(data ? data[0] : null);
		});
	}

}


module.exports = (pool) => {
	return new User(pool);
};