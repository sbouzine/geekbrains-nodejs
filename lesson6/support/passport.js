'use strict';

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;


module.exports = (app, modelUser) => {

	app.use(passport.initialize());
	app.use(passport.session());

	passport.use(new LocalStrategy(
		(username, password, done) => {
			modelUser.auth(username, password, (user) => {
				if(!user)
					return done(null, false, {message: 'Wrong login or password'});
				return done(null, {username: user.id});
			});
		}
	));

	passport.serializeUser((user, done) => {
		done(null, user.username);
	});

	passport.deserializeUser((id, done) => {
		done(null, {username: id});
	});

	return passport.authenticate('local', {
		successRedirect: '/article',
		failureRedirect: '/login',
	});

}
