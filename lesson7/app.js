'use strict';

const path = require('path');
const colors = require('colors');
const bodyParser = require('body-parser');
const templating = require('consolidate');
const jade = require('jade');
const cors = require('cors');
const express = require('express');
const app = express();

// options
const port = 8008;

// load models and controllers
const config = require('./config.js');
const pool = require('./support/mysql.js')(config);
const articleModel = require('./models/article.js')(pool);
const articleController = require('./controllers/article.js')(articleModel);
const articleRestController = require('./controllers/article_rest.js')(articleModel);

// use bodyParser
app.use(bodyParser.urlencoded({extended:true}));

// use jade as templating
app.engine('jade', templating.jade);
app.set('view engine', 'jade');
app.set('views', __dirname + '/templates');

// set public directory with js, css
app.use(express.static(
	path.join(__dirname, 'public')
));


// routes
app.get('/', (req, res) => {
	res.redirect('/client.html');
});

// article routes
app.get('/article', (req, res) => articleController.index(req, res));
app.get('/article/add', (req, res) => articleController.addForm(req, res));
app.get('/article/edit/:id', (req, res) => articleController.editForm(req, res));
app.get('/article/:id', (req, res) => articleController.view(req, res));
app.post('/article/add', (req, res) => articleController.add(req, res));
app.post('/article/edit/:id', (req, res) => articleController.edit(req, res));
app.post('/article/delete/:id', (req, res) => articleController.delete(req, res));

// REST API for article
// app.use(allowCors);
// app.options('/api/*', allowCors);
app.all('/api/*', cors());
app.get('/api/article', (req, res) => articleRestController.get(req, res));
app.get('/api/article/:id', (req, res) => articleRestController.getById(req, res));
app.post('/api/article', (req, res) => articleRestController.post(req, res));
app.put('/api/article/:id', (req, res) => articleRestController.put(req, res));
app.delete('/api/article/:id', (req, res) => articleRestController.delete(req, res));


// start app
console.log('Lesson 7'.bold);
console.log('REST API'.yellow);
console.log('Server started on port ' + port);
app.listen(port);




/*--------------- functions ----------------*/

function allowCors(req, res, next) {
	if(req.headers.origin) {
		res.header('Access-Control-Allow-Origin', req.headers.origin);
		res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE');
	}
	return next();
}
