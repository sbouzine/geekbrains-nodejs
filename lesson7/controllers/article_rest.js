'use strict';


class ArticleRestController {

	constructor(model) {
		this.model = model;
	}

	get(req, res) {
		this.model.list((data) => {
			res.status(200).send({
				articles: data
			});
		});
	}

	getById(req, res) {
		this.model.get(req.params.id, (data) => {
			res.status(200).send({
				article: data
			});
		});
	}

	// post actions
	post(req, res) {
		let article = req.body.article;
		article.user_id = 1;
		this.model.add(article, (insertId) => {
			res.status(200).send({
				message: 'Article add successfuly',
				id: insertId
			});
		});
	}

	put(req, res) {
		this.model.edit(req.params.id, req.body.article, () => {
			res.status(200).send({
				message: 'Article changed successfuly'
			});
		});
	}

	delete(req, res) {
		this.model.delete(req.params.id, () => {
			res.status(200).send({
				message: 'Article removed successfuly'
			});
		});
	}

}



module.exports = (model) => {
	return new ArticleRestController(model);
};