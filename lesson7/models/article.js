'use strict';


class Article {

	constructor(pool) {
		this.db = pool;
		this.table = 'articles';
	}

	list(callback) {
		this.db.query('SELECT * FROM ' + this.table, null, (data) => {
			callback(data);
		});
	}

	get(id, callback) {
		this.db.query('SELECT * FROM ' + this.table + ' WHERE id=?', [id], (data) => {
			callback(data[0]);
		});
	}

	add(data, callback) {
		this.db.query('INSERT INTO ' + this.table + ' SET ?', [data], (result) => {
			callback(result.insertId);
		});
	}

	edit(id, data, callback) {
		this.db.query('UPDATE ' + this.table + ' SET ? WHERE id=?', [data, id], (result) => {
			callback();
		});
	}

	delete(id, callback) {
		this.db.query('DELETE FROM ' + this.table + ' WHERE id=?', [id], (result) => {
			callback();
		});
	}

	// gets fields in table
	fields(callback) {
		this.db.query('SHOW fields FROM ' + this.table, null, (result) => {
			callback(result.map((value) => {
				return value.Field;
			}));
		});
	}

}


module.exports = (pool) => {
	return new Article(pool);
};