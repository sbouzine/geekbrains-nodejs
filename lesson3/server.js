'use strict';

const http = require('http');
const colors = require('colors');
const fs = require('fs');
const request = require('request');
const urlutils = require('url');

// options
const port = 8008;
const form = 'form.html';



console.log('Lesson 3'.bold);
console.log('Yandex.Translate'.yellow);
console.log('Server started on port ' + port);


http.createServer((req, res) => {
	res.writeHead(200, {"Content-type": "text/html"});

	// send form
	res.write(fs.readFileSync(form));

	// get params from url as json
	let params = urlutils.parse(req.url, true).query;

	if(params.query) {
		let url = makeUrl(params.lan, params.query);
		request({url: url, json: true}, (error, response, body) => {
			if(error) {
				console.log(error);
				return;
			}
			res.write(params.query + ' => ' + body.text[0]);
			res.end();
		});
	} else {
		res.end();
	}

}).listen(port);



/*--------------- functions ----------------*/

function makeUrl(lang, text) {

	return urlutils.format({
		protocol: 'https',
		hostname: 'translate.yandex.net',
		pathname: 'api/v1.5/tr.json/translate',
		query: {
			key: 'trnsl.1.1.20140416T130443Z.49db75a946e5d9df.baa803157e4482838c0612cb9c5aa513643049a4',
			lang: lang,
			text: text
		}
	});

}