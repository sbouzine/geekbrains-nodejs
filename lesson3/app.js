'use strict';

const request = require('request');
const cheerio = require('cheerio');
const colors = require('colors');


console.log('Lesson 3'.bold);
console.log('HabraNews\r\n'.yellow);


// set config
let config = {
	hub: 'javascript',
	pages: 1
}

showHabraNews(config);



/*--------------- functions ----------------*/

function showHabraNews(options) {

	// default options
	if(options.hub === undefined)
		options.hub = null;
	if(options.pages === undefined)
		options.pages = 1;
	if(options.page === undefined)
		options.page = 1;

	let url = 'http://habrahabr.ru/';
	url += options.hub ? 'hub/' + options.hub : 'interesting/';
	url += '/page' + options.page;

	request(url, (error, response, body) => {
		if(error) {
			console.log(error);
			return;
		}

		if(response.statusCode != 200)
			return;

		// cheerio with decodeEntities to show cyrillic symbols
		let $ = cheerio.load(body, {decodeEntities: true});

		$('.posts_list .post_title').each(function() {
			console.log($(this).text() + '\r\n' + $(this).attr('href').green + '\r\n');
		});

		// go to next page
		if(++options.page <= options.pages)
			showHabraNews(options);

	});

}