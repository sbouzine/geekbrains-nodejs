'use strict';


class ArticleController {

	constructor(model) {
		this.model = model;
	}

	// get actions
	index(req, res) {
		this.model.list((data) => {
			res.render('article_index', {
				title: 'MySQL Application',
				articles: data
			});
		});
	}

	view(req, res) {
		this.model.get(req.params.id, (data) => {
			res.render('article_view', {
				title: 'MySQL Application',
				article: data
			});
		});
	}

	addForm(req, res) {
		res.render('article_form', {
			title: 'MySQL Application',
			article: {name: '', content: ''},
			action: '/article/add'
		});
	}

	editForm(req, res) {
		this.model.get(req.params.id, (data) => {
			res.render('article_form', {
				title: 'MySQL Application',
				article: data,
				action: '/article/edit/' + data.id
			});
		});
	}

	// post actions
	add(req, res) {
		let article = req.body.article;
		article.user_id = 1;
		this.model.add(article, (insertId) => {
			res.redirect('/article/' + insertId);
		});
	}

	edit(req, res) {
		this.model.edit(req.params.id, req.body.article, () => {
			res.redirect('/article/' + req.params.id);
		});
	}

	delete(req, res) {
		this.model.delete(req.params.id, () => {
			res.redirect('/article');
		});
	}

}



module.exports = (model) => {
	return new ArticleController(model);
};