'use strict';

const path = require('path');
const colors = require('colors');
const bodyParser = require('body-parser');
const templating = require('consolidate');
const jade = require('jade');
const express = require('express');
const app = express();

// options
const port = 8008;

// load models and controllers
const config = require('./config.js');
const pool = require('./support/mysql.js')(config);
const articleModel = require('./models/article.js')(pool);
const articleController = require('./controllers/article.js')(articleModel);

// use bodyParser
app.use(bodyParser.urlencoded({extended:true}));

// use jade as templating
app.engine('jade', templating.jade);
app.set('view engine', 'jade');
app.set('views', __dirname + '/templates');

// set public directory with js, css
app.use(express.static(
	path.join(__dirname, 'public')
));


// routes
app.get('/', (req, res) => {
	res.redirect('/article');
});

// article routes
app.get('/article', (req, res) => articleController.index(req, res));
app.get('/article/add', (req, res) => articleController.addForm(req, res));
app.get('/article/edit/:id', (req, res) => articleController.editForm(req, res));
app.get('/article/:id', (req, res) => articleController.view(req, res));
app.post('/article/add', (req, res) => articleController.add(req, res));
app.post('/article/edit/:id', (req, res) => articleController.edit(req, res));
app.post('/article/delete/:id', (req, res) => articleController.delete(req, res));


// start app
console.log('Lesson 5'.bold);
console.log('Database'.yellow);
console.log('Server started on port ' + port);
app.listen(port);

